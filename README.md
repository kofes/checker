# Google Chrome checker

Google Chrome checker is a python3 script, that easily checks actual version of Google Chrome browser for Windows 7/8/10 with arch x32/x64.

##### Script contains a class `ChromeUpdater` with update's methods:

###### `updateCurrentVersion()`
* Returns: 
    `Error`/nothing
* Description:
    Sets ``current_version`` = "installed version". It checks directory **%USERPROFILE%**\AppData\Local\Google\Chrome\Application (a.k.a `appPath`).
If it's not exist => Google Chrome has not been installed
 -> method returns Error.LOCAL_CHROME;
Else it contains a folder with the name that is the current version number of Google Chrome.
 -> method sets `current_version`, returns nothing.

###### `updateUrl()`
* Returns:
    `Error`/nothing
* Description:
    Method checks actual Desktop version from [chromeblog](https://chromereleases.googleblog.com/search/label/Stable%20updates "Chrome Releases: Stable updates"). It's scraping page using [requests](http://docs.python-requests.org/en/master/ "Requests main page") and [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/ "Beautiful Soup main page") libraries with standard library `re` to get last release version from posts.
If response code != 200
 -> method returns Error.CONNECTION;
Else
 -> method sets `lastest_version`, returns nothing.

###### `updateUrl()`
* Returns:
    `Error`/nothing
* Description: 
    Method parses `manifest` file from `appPath` to identify the chosen architecture(x32/x64)  (a.k.a `arch_type`) (if `manifest` is not exists -> `arch_type` is set to current architecture). After that it sets `chromeApp_url` using `arch_type`.
    
**Url** was created on the basis of the analyzed direct links to Windows standalone versions from 2013/2014/2017 years and it includes only `appguid` and `arch_type` params, that remained the same all of this time.

##### Also class includes getters, but them have the same names and two methods for manipulations with installer:
###### `downloadFile()`
* Returns:
    `Error`/installer's filename
* Description:
    Method tries to download latest installer from `chromeApp_url` to "googlechrome.exe"
###### `runInstaller(mode)`
* Returns:
    `Error`/return_code
* Description:
    Method tries to run "googlechrome.exe" with silent mode = `mode` like subprocess. If installer exits with return_code
     -> Method returns return_code.