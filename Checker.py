import os
import re
import requests
from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import platform
import subprocess
from distutils.util import get_platform
import enum

class Error(enum.Enum):
    NONE = 0,
    OS = 1,
    LOCAL_CHROME = 2,
    CONNECTION = 3,
    LOCAL_INSTALLER = 4

class ChromeUpdater:
    def __init__(self):
        self.current_version = ''
        self.latest_version = ''
        self.appPath = ''
        self.chromeApp_url = ''
    def updateCurrentVersion(self):
        #Check OS
        if (platform.system() != 'Windows'):
            print('Script only for Windows 7/8/10')
            return Error.OS
        #Find and set Google Chrome directory
        user_dir = os.path.expanduser('~')
        self.appPath = '{0}\AppData\Local\Google\Chrome\Application'.format(user_dir)
        if (not os.path.exists(self.appPath)):
            print('Can\'t find directory of local chrome')
            return Error.LOCAL_CHROME
        #Find directory of current version
        res = [str for str in os.listdir(self.appPath) if (re.match(r'^(\d+\.){3}\d+$', str) != None)]
        #Set 'current_version'
        self.current_version = res[0]
    def updateUrl(self):
        #Check OS
        if (platform.system() != 'Windows'):
            print('Script only for Windows 7/8/10')
            return Error.OS
        #Read manifest(xml)
        manifest_file = '{0}\\{1}\\{1}.manifest'.format(self.appPath, self.current_version)
        arch_type = ''
        if (os.path.isfile(manifest_file)):
            manifest = ET.ElementTree(file=manifest_file)
            #Check downloaded version(architecture)
            root = manifest.getroot()
            for child in root.iter():
                if (child.get('type') != None):
                    arch_type = child.get('type')
                    break
        else:
            arch_type = get_platform()
        #Set download url
        win_version = ''
        if (arch_type == 'win32'):
            win_version = 'stable-arch_x86'
        else:
            win_version = 'x64-stable';

        self.chromeApp_url = 'https://dl.google.com/tag/s/appguid=%7B8A69D345-D564-463C-AFF1-A69D9E530F96%7D%26ap={0}/update2/installers/ChromeStandaloneSetup.exe'.format(win_version)
    def updateLatestVersion(self):
        #Check OS
        if (platform.system() != 'Windows'):
            print('Script only for Windows 7/8/10')
            return Error.OS
        #Download page with 'latest_version'
        src = requests.get(self.__chromeBlog__())
        if (src.status_code != 200):
            print('Can\'t check newest version')
            return Error.CONNECTION
        #Finding posts with latest versions
        page_soup = BeautifulSoup(src.text, "lxml")
        blog = page_soup.find('div', {'class': 'widget Blog'})
        posts = blog.find_all('div', {'class': 'post'})
        #Finding latest post with version number for Desktop
        latest_desktopRelease_post = ''
        for post in posts:
            if (re.search(r'Desktop', post.find('h2', {'class': 'title'}).text) != None):
                latest_desktopRelease_post = post
                date = post.find('span', {'class', 'publishdate'}).text
                break
        #Find content of current post
        latest_desktopRelease_block = latest_desktopRelease_post.\
                  find('div', {'class': 'post-body'}).\
                  find('div', {'itemprop': 'articleBody'})
        self.latest_version = re.search(r'(\d+\.){3}\d+', latest_desktopRelease_block.text).group(0)
    def downloadFile(self):
        #Check OS
        if (platform.system() != 'Windows'):
            print('Script only for Windows 7/8/10')
            return Error.OS
        localFilename = "googlechrome.exe"
        r = requests.get(self.url(), stream = True)
        with open(localFilename, "wb") as f:
            for chunk in r.iter_content(chunk_size = 1024):
                if chunk:
                    f.write(chunk)
        return localFilename
    def runInstaller(self, mode='silent'):
        #Check OS
        if (platform.system() != 'Windows'):
            print('Script only for Windows 7/8/10')
            return Error.OS
        #Set mode of installation
        if (mode == 'silent'):
            mode = '/silent'
        else:
            mode = ''
        filename = "googlechrome.exe {0} /install".format(mode)
        #Running installer in subprocess(detectin' errors)
        if (os.path.isfile(filename)):
            return subprocess.call(filename, shell=False)
        return Error.LOCAL_INSTALLER
    def __chromeBlog__(self):
        return 'https://chromereleases.googleblog.com/search/label/Stable updates'
    def currentVersion(self):
        return self.current_version
    def latestVersion(self):
        return self.latest_version
    def url(self):
        return self.chromeApp_url


test = ChromeUpdater()
test.updateCurrentVersion()
test.updateLatestVersion()
test.updateUrl()
print('Current version: {0}\n Latest version: {1}\n Download link: {2}'.format(test.currentVersion(), test.latestVersion(), test.url()))
